# Todo Bash

<center>

![](https://codeberg.org/_-Caleb-_/todo-bash/raw/branch/master/appimage/bashtodo.AppDir/usr/share/icons/hicolor/128x128/apps/todo.png)

</center>

[Fork](https://github.com/Lateralus138/todo-bash) de este programa de lista de tareas creado en Bash Scripting que utilizo junto con el [script de quotes](https://codeberg.org/_-Caleb-_/Bash_Citas) que incluye lo siguiente respecto a la [versión original de GitHub](https://github.com/Lateralus138/todo-bash)):

- Eliminación de scripts duplicados del repositorio.
- Traducción al español.
- Añadidos algunos colores para la salida en la consola.
- Añadidos nuevos estilos en la lista de tareas haciéndola más amigable.

![](https://codeberg.org/_-Caleb-_/todo-bash/raw/branch/master/images/Captura%20de%20pantalla%20de%202022-07-10%2010-33-38.png)

---------------------------
## Salida de la consola 
~~~~ @Uso:	todo [ÍNDICE]...

     	todo [OPCIONES [ÍNDICE|TAREAS]...]...

Listar, añadir o eliminar tareas.


@OPCIONES:
	-h,--help	Este mensaje de ayuda.
	-r,--remove	Elimina una tarea por su número de ÍNDICE.
	-a,--add	Añade una TAREA.
	-q,--quiet	No mostrar los mensajes de error.
@ÍNDICE:
	Integers	Indíce de tareas
@TAREAS:
	String	        Tareas.
@EJEMPLOS:
	todo
	        Lista todas las tareas.
	todo 1
	        Lista la tarea INDEXADA como #1.
	todo -a "Algo que hacer"
	        Añade una tarea.
	todo -r 1
	        Elimina la tarea INDEXADA como  #1.
~~~~

## En planificación:

- Opción **"-c | --clear"** para eliminar todas las tareas.
- Opción **"-u | --update"** para actualizar el script desde este repositorio.
